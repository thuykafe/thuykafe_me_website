from django.apps import AppConfig


class RestApiConfig(AppConfig):
    name = 'rest_api'
    label = 'thuykafe_me.rest_api'
    verbose_name = 'REST api contains authetication'
